/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morixin.processor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.morixin.models.Condition;
import com.morixin.models.Facility;
import com.morixin.models.IndividualReport;
import com.morixin.models.MessageHeader;
import com.morixin.models.Metadata;
import com.morixin.models.PatientDemographics;
import com.morixin.models.ViralLoadTestReport;

/**
 *
 * @author morrison idiasirue
 * @email morrison.idiasirue@gmail.com
 */
public class DataFiller {
    
    public static Metadata populateData(){
    // here are some dummy data
        
        Metadata mData = new Metadata();
        
        // populate the viralLoadTestReport object
        ViralLoadTestReport vrtestReport = new ViralLoadTestReport();
        vrtestReport.setApprovalDate(new Date());
        vrtestReport.setAssayDate(new Date());
        vrtestReport.setDateSampleRecievedAtPCRLab(new Date());
        vrtestReport.setLaboratoryRegistrationNumber("l780");
        vrtestReport.setOrderedTestDate(new Date());
        vrtestReport.setPcrLabSampleNumber("testsam001");
        vrtestReport.setResultDate(new Date());
        vrtestReport.setSampleCollectionDate(new Date());
        vrtestReport.setSampleType("s45902");
        vrtestReport.setViralLoadIndication("test_indicator");
        vrtestReport.setViralLoadResult("test_result");
        vrtestReport.setVisitDate(new Date());
        
        //populate the condition object
        Condition cond = new Condition();
        cond.setConditionCode("test_condition_code");
        List<ViralLoadTestReport> viralLoadList = new ArrayList<>();
        viralLoadList.add(vrtestReport);
        
        cond.setViralLoadTestReport(viralLoadList);
        
        //populate patientDemoGraphics object
        PatientDemographics patientDm = new PatientDemographics();
        patientDm.setHospitalNumber("h20391");
        patientDm.setPatientDateOfBirth(new Date());
        patientDm.setPatientIdentifier("test_idn");
        patientDm.setPatientSexCode("m");
        //create and populate treatmentFality object
        Facility treatmentFacility = new Facility();
        treatmentFacility.setFacilityID("tr_002");
        treatmentFacility.setFacilityName("test facility name");
        treatmentFacility.setFacilityTypeCode("test_code");
        
        patientDm.setTreatmentFacility(treatmentFacility);
        
        //create and populate IndividualReport
        IndividualReport individualReport = new IndividualReport();
        individualReport.setCondition(cond);
        individualReport.setPatientDemographics(patientDm);
        
        
        // populate messageHeader object
        MessageHeader msgHeader = new MessageHeader();
        msgHeader.setMessageCreationDateTime(new Date());
        msgHeader.setMessageSchemaVersion("sch_001");
        msgHeader.setMessageStatusCode("200");
        msgHeader.setMessageUniqueID("test_uuid");
        
        Facility msgSendOrg = new Facility();
        msgSendOrg.setFacilityID("tr_003");
        msgSendOrg.setFacilityName("test msg sending org");
        msgSendOrg.setFacilityTypeCode("org0923");
        
        msgHeader.setMessageSendingOrganization(msgSendOrg);
        
        //now populate the metaData
        mData.setCondition(cond);
        mData.setIndividualReport(individualReport);
        mData.setMessageHeader(msgHeader);
        
        
        return mData;
        
    }
}
