/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morixin.models;

/**
 *
 * @author morrison
 */
public class IndividualReport {
    private PatientDemographics patientDemographics;
    private Condition condition;

    public PatientDemographics getPatientDemographics() {
        return patientDemographics;
    }

    public void setPatientDemographics(PatientDemographics patientDemographics) {
        this.patientDemographics = patientDemographics;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }
    
    
}
