/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morixin.models;

import java.util.Date;

/**
 *
 * @author morrison idiasirue
 * @email morrison.idiasirue@gmail.com
 */
public class MessageHeader {
    private String messageStatusCode;
    private Date messageCreationDateTime;
    private String messageSchemaVersion;
    private String messageUniqueID;
    private Facility messageSendingOrganization;

    public String getMessageStatusCode() {
        return messageStatusCode;
    }

    public void setMessageStatusCode(String messageStatusCode) {
        this.messageStatusCode = messageStatusCode;
    }

    public Date getMessageCreationDateTime() {
        return messageCreationDateTime;
    }

    public void setMessageCreationDateTime(Date messageCreationDateTime) {
        this.messageCreationDateTime = messageCreationDateTime;
    }

    public String getMessageSchemaVersion() {
        return messageSchemaVersion;
    }

    public void setMessageSchemaVersion(String messageSchemaVersion) {
        this.messageSchemaVersion = messageSchemaVersion;
    }

    public String getMessageUniqueID() {
        return messageUniqueID;
    }

    public void setMessageUniqueID(String messageUniqueID) {
        this.messageUniqueID = messageUniqueID;
    }

    public Facility getMessageSendingOrganization() {
        return messageSendingOrganization;
    }

    public void setMessageSendingOrganization(Facility messageSendingOrganization) {
        this.messageSendingOrganization = messageSendingOrganization;
    }
    
    
    
    
}
