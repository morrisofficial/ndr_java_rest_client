/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morixin.models;

/**
 *
 * @author morrison idiasirue
 * @email  morrison.idiasirue@gmail.com
 */
public class Metadata {
    private MessageHeader messageHeader;
    private IndividualReport individualReport;
    private Condition condition;

    public MessageHeader getMessageHeader() {
        return messageHeader;
    }

    public void setMessageHeader(MessageHeader messageHeader) {
        this.messageHeader = messageHeader;
    }

    public IndividualReport getIndividualReport() {
        return individualReport;
    }

    public void setIndividualReport(IndividualReport individualReport) {
        this.individualReport = individualReport;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }
    
    
}
