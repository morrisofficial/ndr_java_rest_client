/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morixin.models;

import java.util.Date;

/**
 *
 * @author morrison idiasirue
 * @email morrison.idiasirue@gmail.com
 */
public class PatientDemographics {
     private String patientIdentifier;
     private String hospitalNumber;
     private Date patientDateOfBirth;
     private String patientSexCode;
     private Facility treatmentFacility;

    public String getPatientIdentifier() {
        return patientIdentifier;
    }

    public void setPatientIdentifier(String patientIdentifier) {
        this.patientIdentifier = patientIdentifier;
    }

    public String getHospitalNumber() {
        return hospitalNumber;
    }

    public void setHospitalNumber(String hospitalNumber) {
        this.hospitalNumber = hospitalNumber;
    }

    public Date getPatientDateOfBirth() {
        return patientDateOfBirth;
    }

    public void setPatientDateOfBirth(Date patientDateOfBirth) {
        this.patientDateOfBirth = patientDateOfBirth;
    }

    public String getPatientSexCode() {
        return patientSexCode;
    }

    public void setPatientSexCode(String patientSexCode) {
        this.patientSexCode = patientSexCode;
    }

    public Facility getTreatmentFacility() {
        return treatmentFacility;
    }

    public void setTreatmentFacility(Facility treatmentFacility) {
        this.treatmentFacility = treatmentFacility;
    }
     
     
     
}
