/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morixin.models;

import java.util.Date;

/**
 *
 * @author morrison idiasirue
 * @email morrison.idiasirue@gmail.com
 */
public class ViralLoadTestReport {
         private String laboratoryRegistrationNumber;
         private String pcrLabSampleNumber;
         private Date visitDate;
         private Date orderedTestDate;
         private Date sampleCollectionDate;
         private String sampleType;
         private String viralLoadIndication;
         private Date dateSampleRecievedAtPCRLab;
         private String viralLoadResult;
         private Date resultDate;
         private Date assayDate;
         private Date approvalDate;

    public String getLaboratoryRegistrationNumber() {
        return laboratoryRegistrationNumber;
    }

    public void setLaboratoryRegistrationNumber(String laboratoryRegistrationNumber) {
        this.laboratoryRegistrationNumber = laboratoryRegistrationNumber;
    }

    public String getPcrLabSampleNumber() {
        return pcrLabSampleNumber;
    }

    public void setPcrLabSampleNumber(String pcrLabSampleNumber) {
        this.pcrLabSampleNumber = pcrLabSampleNumber;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public Date getOrderedTestDate() {
        return orderedTestDate;
    }

    public void setOrderedTestDate(Date orderedTestDate) {
        this.orderedTestDate = orderedTestDate;
    }

    public Date getSampleCollectionDate() {
        return sampleCollectionDate;
    }

    public void setSampleCollectionDate(Date sampleCollectionDate) {
        this.sampleCollectionDate = sampleCollectionDate;
    }

    public String getSampleType() {
        return sampleType;
    }

    public void setSampleType(String sampleType) {
        this.sampleType = sampleType;
    }

    public String getViralLoadIndication() {
        return viralLoadIndication;
    }

    public void setViralLoadIndication(String viralLoadIndication) {
        this.viralLoadIndication = viralLoadIndication;
    }

    public Date getDateSampleRecievedAtPCRLab() {
        return dateSampleRecievedAtPCRLab;
    }

    public void setDateSampleRecievedAtPCRLab(Date dateSampleRecievedAtPCRLab) {
        this.dateSampleRecievedAtPCRLab = dateSampleRecievedAtPCRLab;
    }

    public String getViralLoadResult() {
        return viralLoadResult;
    }

    public void setViralLoadResult(String viralLoadResult) {
        this.viralLoadResult = viralLoadResult;
    }

    public Date getResultDate() {
        return resultDate;
    }

    public void setResultDate(Date resultDate) {
        this.resultDate = resultDate;
    }

    public Date getAssayDate() {
        return assayDate;
    }

    public void setAssayDate(Date assayDate) {
        this.assayDate = assayDate;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }
         
         
         
}
