/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morixin.models;

import java.util.List;

/**
 *
 * @author morrison
 */
public class Condition {
    private String conditionCode;
    private List<ViralLoadTestReport> viralLoadTestReport;

    public String getConditionCode() {
        return conditionCode;
    }

    public void setConditionCode(String conditionCode) {
        this.conditionCode = conditionCode;
    }

    public List<ViralLoadTestReport> getViralLoadTestReport() {
        return viralLoadTestReport;
    }

    public void setViralLoadTestReport(List<ViralLoadTestReport> viralLoadTestReport) {
        this.viralLoadTestReport = viralLoadTestReport;
    }
    
    
}
