/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morixin.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.morixin.models.Metadata;
import com.morixin.processor.DataFiller;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.TimeZone;


/**
 *
 * @author morrison Idiasirue
 * @email morrison.idiasirue@gmail.com
 */
public class JerseyClient {
    public static void main(String args[]){
    
         String url = "http://ndr-api-test.shieldnigeriaproject.com/api/BaseData";
        String name = "REPLACE_WITH_USERNAME";
        String password = "REPLACE_WITH_PASSWORD";
        String authString = name + ":" + password;
        String authStringEncoded = Base64.getEncoder().encodeToString(authString.getBytes());   
        
 
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
       // mapper.setDateFormat(df);
       
       //edit the populate method to use your own data.
        Metadata metadata = DataFiller.populateData();
        
        try{
             Client restClient = Client.create();
        WebResource webResource = restClient.resource(url);
            System.out.println(mapper.writeValueAsString(metadata));
        ClientResponse resp = webResource.type("application/json")
                                        .header("Authorization", "Basic " + authStringEncoded)
                                         //.get(ClientResponse.class);
                                         .post(ClientResponse.class, mapper.writeValueAsString(metadata));
          if(resp.getStatus() != 200){
            System.err.println("Unable to connect to the server");
       }
        String output = resp.getEntity(String.class);
        System.out.println("response: "+output);
        
        }catch(Exception ex){
            ex.printStackTrace();
        }
   
                                       
      
        
    }
}
